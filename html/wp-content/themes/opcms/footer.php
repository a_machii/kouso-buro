    <footer>
      <div id="footer">
        <!--pcのみ表示-->
        <div class="unit pc">
          <ul>
            <li><a href="<?php echo home_url('/'); ?>">HOME</a></li>
            <li><a href="<?php echo home_url('/'); ?>about/">酵素風呂とは</a></li>
            <li><a href="<?php echo home_url('/'); ?>about#feature">特徴の効能</a></li>
            <li><a href="<?php echo home_url('/'); ?>method/">入浴の仕方</a></li>
            <li><a href="<?php echo home_url('/'); ?>shop#price">料金のご案内</a></li>
            <li><a href="<?php echo home_url('/'); ?>shop/">店舗のご案内</a></li>
            <li><a href="<?php echo home_url('/'); ?>power/">酵素のちから</a></li>
          </ul>
          <p>copyright(c) イオンハウス狭山 all right reserved</p>
        </div><!--/.unit-->
        <p class="to_top">
          <a href="#wrap" class="ophv"><img src="<?php echo home_url('/'); ?>common/img/to_top_btn.gif" height="57" width="58" alt="トップへ戻る"></a>
        </p>

        <!--rspのみ表示-->
        <p class="copyright rsp">
          copyright(c) イオンハウス狭山 all right reserved
        </p>
      </div><!--/#footer-->
    </footer>
  </div><!--/#contents-->

  <!-- script -->
  <script type="text/javascript" src="<?php echo home_url('/'); ?>common/js/rollover/rollover.js"></script>
  <script type="text/javascript" src="<?php echo home_url('/'); ?>common/js/rollover/opacity-rollover2.1.js"></script>
  <script type="text/javascript" src="<?php echo home_url('/'); ?>common/js/smoothScrollEx.js"></script>

  <!--ロールオーバーの記述-->
  <script type="text/javascript">
    jQuery(document).ready(function($) {
      $('.ophv').opOver(1.0, 0.6, 200, 200);
    });
  </script>

  <!--レスポンシブのメニュー記述-->
  <script type="text/javascript">
    $(document).ready(function(){
      $(window).resize(function() {
          var w = $(window).width();
          if(1000 <= w){
            $('#menu').css("display","none");
          }
        });
        $('#menu_btn').on('click',function(){
          $('#menu').slideToggle();
      });
    });
  </script>

<?php wp_footer(); //</body>の直前に記述する ?>
</body>
</html>


