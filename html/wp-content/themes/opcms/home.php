<?php get_header(); ?>

    <main>
      <div id="wrap" class="top">
        <div class="section">
          <p class="about"><a href="<?php echo home_url('/'); ?>about/" class="ophv">
            <!--pc表示-->
            <img src="<?php echo home_url('/'); ?>common/img/top/about_img.jpg" height="300" width="500" alt="酵素風呂とは" class="ophv pc">
            <!--rsp表示-->
            <img src="<?php echo home_url('/'); ?>common/img/sp/about_img.jpg" height="225" width="375" alt="酵素風呂とは" class="ophv rsp">
          </a></p>
          <p class="feature"><a href="<?php echo home_url('/'); ?>about#feature">
            <!--pc表示-->
            <img src="<?php echo home_url('/'); ?>common/img/top/feature_img.jpg" height="300" width="264" alt="特徴と効能" class="ophv pc">
            <!--rsp表示-->
            <img src="<?php echo home_url('/'); ?>common/img/sp/feature_img.jpg" height="224" width="380" alt="特徴と効能" class="ophv rsp">
          </a></p>
        </div><!--/.section-->

        <!--pc表示-->
        <div class="section pc">
          <p class="power"><a href="<?php echo home_url('/'); ?>power/"><img src="<?php echo home_url('/'); ?>common/img/top/power_img.jpg" height="432" width="267" alt="酵素のちから" class="ophv"></a></p>
          <div class="unit">
            <section>
              <div class="news">
                <h2><img src="<?php echo home_url('/'); ?>common/img/top/news_ttl.png" height="16" width="238" alt="イオンハウス狭山からのお知らせ"></h2>
                <div class="article_area">

<?php
  $args = array(
    'post_type' => 'news',
    'taxonomy' => 'newscategory',
    'posts_per_page' => -1,
  );

  $my_posts = get_posts( $args );
  global $post;
  foreach ( $my_posts as $post ):
  setup_postdata( $post );
?>

                  <article>
                    <h3><?php the_title(); ?></h3>
                    <p><?php the_field('news_article'); ?></p>
                  </article>

<?php
  endforeach;
  wp_reset_postdata();
?>
                </div><!--/.article_area-->
              </div><!--/.news-->
            </section>

            <div class="shop">
              <p class="ophv"><a href="<?php echo home_url('/'); ?>shop/">店舗のご案内</a></p>
            </div><!--/.shop-->
            <div class="price">
              <p class="ophv"><a href="<?php echo home_url('/'); ?>shop#price">料金のご案内</a></p>
            </div><!--/.price-->
          </div><!--/.unit-->
        </div><!--/.section pc-->

        <!--rsp表示-->
        <div class="section_rsp rsp">
          <p class="power">
            <a href="<?php echo home_url('/'); ?>power/" class="ophv"><img src="<?php echo home_url('/'); ?>common/img/sp/power_img.jpg" height="222" width="375" alt="酵素のちから"></a>
          </p>
        </div><!--/.section rsp-->

        <div class="rsp section_rsp">
          <div class="shop">
            <p class="ophv"><a href="<?php echo home_url('/'); ?>shop/">店舗のご案内</a></p>
          </div><!--/.shop-->
          <div class="price">
            <p class="ophv"><a href="<?php echo home_url('/'); ?>shop#price">料金のご案内</a></p>
          </div><!--/.price-->
        </div><!--/.section_rsp rsp-->

        <section>
          <div class="rsp section_rsp">
            <div class="news">
              <h2><img src="<?php echo home_url('/'); ?>common/img/top/news_ttl.png" height="16" width="238" alt="イオンハウス狭山からのお知らせ"></h2>
              <div class="article_area">
<?php if(have_posts()): while(have_posts()): the_post(); ?>
                  <article>
<?php if(isFirst()): ?>
                    <h3 class="first"><?php the_title(); ?></h3>
<?php else: ?>
                    <h3><?php the_title(); ?></h3>
<?php endif; ?>
                    <p><?php the_field('news_article'); ?></p>
                  </article>

<?php endwhile; endif; ?>

<?php wp_reset_postdata(); ?>
              </div><!--/.article_area-->
            </div><!--/.news-->
          </div><!--/rsp section_rsp-->
        </section>

        <section>
          <div class="section" id="banner">
            <h2 class="banner_ttl"><img src="<?php echo home_url('/'); ?>common/img/top/banner_ttl.png" height="23" width="340" alt="森林浴気分で心身ともにすっきり"></h2>
            <p>おがくず酵素風呂とは桧のおがくずに糠と数十種類の植物で発酵させた酵素を混ぜて熟成し、７０℃まで自然発酵させた乾式のお風呂のことです。<br>身体の内側からゆっくり温まることで、新陳代謝を促し、美容と健康に良いと好評をいただいております。ぜひ一度イオンハウス狭山へお越しください。</p>
          </div><!--/.section-->
        </section>

        <!--rspのみ表示-->
        <aside>
          <div class="section rsp" id="banner_rsp">
            <a href="tel:0312345678"><img src="<?php echo home_url('/'); ?>common/img/sp/info_ban.jpg" height="111" width="351" alt="お問合せ・ご相談はこちら"></a>
          </div><!--/.section rsp-->
        </aside>

        <aside>
          <div class="section" id="shopinfo">
            <p><span>イオンハウス狭山</span>　〒350-1312 埼玉県狭山市堀兼1970-3　［<a href="https://www.google.co.jp/maps/place/%E3%80%92350-1312+%E5%9F%BC%E7%8E%89%E7%9C%8C%E7%8B%AD%E5%B1%B1%E5%B8%82%E5%A0%80%E5%85%BC%EF%BC%91%EF%BC%99%EF%BC%97%EF%BC%90%E2%88%92%EF%BC%93/@35.8487623,139.4464243,17z/data=!3m1!4b1!4m5!3m4!1s0x6018dc0052c3f3ef:0x20818597cad3ee73!8m2!3d35.8487623!4d139.448613" target="_blank" class="ophv">地図はこちら</a>］</p>
            <p><span>電話</span>：04-2959-3087／<span>FAX</span>：04-2959-3002</p>
            <p><span>営業時間</span>　平日：10:00～20:00（最終受付18:30） 土日祝：10:00～20:00（最終受付18:30）</p>
            <p><span>定休日</span>　　月・金曜日</p>
          </div><!--/.shopinfo-->
        </aside>
      </div><!--/#wrap-->
    </main>

<?php get_footer(); ?>