<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
<?php
  global $this_pagetitle;
  global $this_page_keywd;
  global $this_page_desc;
?>
  <meta charset="<?php bloginfo('charset'); ?>">
  <title><?php if($this_pagetitle) : echo $this_pagetitle. ' | '; endif; ?>埼玉県狭山市のおがくず酵素風呂 大高酵素 イオンハウス狭山</title>
  <meta name="description" content="<?php if($this_page_desc) : echo $this_page_desc. ' - '; elseif($this_pagetitle) : echo $this_pagetitle. ' - '; endif; ?>埼玉県狭山市のおがくず酵素風呂・イオンハウス狭山です。大高酵素を使用した酵素風呂でダイエット、デトックス、リラックス" >
  <meta name="keywords" content="<?php if($this_page_keywd) : echo $this_page_keywd. ','; elseif($this_pagetitle) : echo $this_pagetitle. ','; endif; ?>酵素風呂,大高酵素,イオンハウス,埼玉,おがくず,ダイエット,デトックス" >
  <meta name="format-detection" content="telephone=no,address=no,email=no">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width,initial-scale=1.0">
  <link rel="stylesheet" href="<?php echo home_url('/'); ?>common/css/style.css">
  <link rel="stylesheet" type="text/css" href="<?php echo home_url('/'); ?>common/css/reset.css">
    <!--&#91;if lt IE 9&#93;>
  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
  <!&#91;endif&#93;-->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<?php if(is_home()): ?>
  <script type="text/javascript" src="<?php echo home_url('/'); ?>common/js/jquery.bxslider.min.js"></script>
<?php endif; ?>

  <!--header main_img可変設定の記述-->
  <script type="text/javascript">
    $(function(){
      $(window).load(function(){
        var w1 = window.innerWidth; //スクロールを含むウインド幅
        var w2 = document.body.clientWidth; //スクロールを含まないウインド幅
        var w3 = (w1-w2) + 867; //スクロール幅 + contens幅（固定）
        var result = w1-w3;
          $('#header').css({'width':result});
          $('#main_img').css({'width':result});
          $('#main_img .slider li').css({'width':result});
      });

      $(window).resize(function(){
        var timer = false;
        var w1 = window.innerWidth;
        var w2 = document.body.clientWidth;
        var w3 = (w1-w2) + 867;
        var result = w1-w3;
        if (timer !== false) {
          clearTimeout(timer);
        }
        timer = setTimeout(function() {
        console.log('resized');
          $('#header').css({'width':result});
          $('#main_img').css({'width':result});
          $('#main_img .slider li').css({'width':result});
        }, 200);
      });
    });
  </script>

<?php if(is_home()): ?>
  <!--bxsliderの記述-->
  <script type="text/javascript">
    $(document).ready(function() {
        $('.slider').bxSlider({
            mode: 'fade',
            auto: true,
            controls: false, //prev/nextを非表示
            speed: 4000,
            pause: 8000, //静止時間
            responsive: true
        });
    });
  </script>
<?php endif; ?>

  <!--レスポンシブ-->
<?php if(is_home()): ?>
  <script type="text/javascript">
    $(window).on('load resize', function(){

      // ヘッダーの高さ取得
      var header_w = document.body.clientWidth;
      var header_h = header_w * 0.87;
      $("#header_rsp").css("width", header_w);
      $("#header_rsp").css("height", header_h);

      // メニューボタンの高さ取得
      var menu_h = $('#header_rsp #menu_btn').innerHeight();
      $("#contents #menu").css("top", menu_h + "px");

      // 店舗・料金のご案内の高さ取得
      var shop_w1 = $('#contents .top .section_rsp .shop').innerWidth();
      var shop_h1 = shop_w1 * 0.9;
      $('#contents .top .section_rsp .shop').css('height', shop_h1);
      $('#contents .top .section_rsp .price').css('height', shop_h1);

      // お知らせの高さ取得
      var news_w = $('#contents .top .section_rsp .news').innerWidth();
      var news_h = news_w * 0.58;
      var article_area_h = news_h * 0.7;
      $('#contents .top .section_rsp .news').css('height', news_h);
      $('#contents .top .section_rsp .news .article_area').css('height', article_area_h);

      // レスポンシブ画像
      var banner_w = $('#contents .top #banner').width();
      if (banner_w < 340){
        $('#contents .top #banner .banner_ttl').css({
          'width': banner_w,
          'height': 'auto'
          });
      } else{
        $('#contents .top #banner .banner_ttl').css({
          'width': 340 + 'px',
          'height': 23 + 'px'
        });
      };
    });
  </script>

<?php elseif(is_page(array('shop','method'))): ?>
  <script type="text/javascript">
    $(window).on('load resize', function(){

      // ヘッダーの高さ取得
      var header_w = document.body.clientWidth;
      var header_h = header_w * 0.87;
      $("#header_rsp").css("width", header_w);
      $("#header_rsp").css("height", header_h);

      // メニューボタンの高さ取得
      var menu_h = $('#header_rsp #menu_btn').innerHeight();
      $("#contents #menu").css("top", menu_h + "px");
    });
  </script>

<?php elseif(is_page(array('about'))): ?>
  <script type="text/javascript">
    $(window).on('load resize', function(){

      // ヘッダーの高さ取得
      var header_w = document.body.clientWidth;
      var header_h = header_w * 0.87;
      $("#header_rsp").css("width", header_w);
      $("#header_rsp").css("height", header_h);

      // メニューボタンの高さ取得
      var menu_h = $('#header_rsp #menu_btn').innerHeight();
      $("#contents #menu").css("top", menu_h + "px");

      // レスポンシブ画像
      var ol_w  = $('#contents .btm .section').width();
      if (ol_w < 464){
        $('#contents .about #outline .ol_img').css({
          'width': ol_w,
          'height': 'auto'
        });
      } else{
        $('#contents .about #outline .ol_img').css({
          'width': 464 + 'px',
          'height': 288 + 'px'
        });
      };
    });
  </script>

<?php elseif(is_page('power')): ?>
  <!--レスポンシブ-->
  <script type="text/javascript">
    $(window).on('load resize', function(){

      // ヘッダーの高さ取得
      var header_w = document.body.clientWidth;
      var header_h = header_w * 0.87;
      $("#header_rsp").css("width", header_w);
      $("#header_rsp").css("height", header_h);

      // メニューボタンの高さ取得
      var menu_h = $('#header_rsp #menu_btn').innerHeight();
      $("#contents #menu").css("top", menu_h + "px");

      // text_area02の幅を取得
      var section_w = $('#contents .btm .section').width();
      var text_area02a = section_w;
      var text_area02b = {
        width: '600px',
        margin: ' 0 auto'
      };
      var all_w = window.innerWidth;
      if(text_area02a <= 600){
        $('#contents .power #guide .unit .txt_area02').css('width', text_area02a);
      } else if(all_w >= 1001){
        $('#contents .power #guide .unit .txt_area02').css('width', 508);
      } else if(text_area02a >= 600 && text_area02a < 1001){
        $('#contents .power #guide .unit .txt_area02').css(text_area02b);
      }
    });
  </script>
<?php endif; ?>

  <!--google analyticsの記述-->
  <script type="text/javascript">
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-23425015-1']);
    _gaq.push(['_trackPageview']);

    (function() {
      var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
      ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();
  </script>

  <?php wp_head(); ?>
</head>

<body>
  <header>
    <!--pc表示-->
    <div id="header" class="pc">
      <h1>埼玉県狭山市の酵素風呂 大高酵素 イオンハウス狭山</h1>
      <p class="logo"><a href="<?php echo home_url('/'); ?>"><img src="<?php echo home_url('/'); ?>common/img/logo.png" height="260" width="126" alt="イオンハウス狭山"></a></p>
    </div><!--/#header-->

    <!--rsp表示-->
    <div id="header_rsp" class="rsp">
      <h1><img src="<?php echo home_url('/'); ?>common/img/logo.png" height="260" width="126" alt="イオンンハウス狭山"><a href="<?php echo home_url('/'); ?>"></a></img></h1>
      <p id="menu_btn" class="rsp"><img src="<?php echo home_url('/'); ?>common/img/sp/menu.btn.gif" height="43" width="46" alt="メニュー"></p>
    </div><!--/#header_rsp-->

    <!--pc表示-->
    <div id="main_img" class="pc">
      <ul class="slider">
        <li style="background-image:url('<?php echo home_url('/'); ?>common/img/main01.jpg');"></li>
<?php if(is_home()): ?>
        <li style="background-image:url('<?php echo home_url('/'); ?>common/img/main02.jpg');"></li>
<?php endif; ?>
      </ul>
    </div><!--/.main_img-->
  </header>

  <div id="contents">
    <nav>
      <!--pc表示-->
      <ul id="gnav" class="pc">
        <li><a href="<?php echo home_url('/'); ?>about/"><img src="<?php echo home_url('/'); ?>common/img/about_off.gif" height="106" width="16" alt="酵素風呂とは" onmouseover="this.src='<?php echo home_url('/'); ?>common/img/about_on.gif'" onmouseout="this.src='<?php echo home_url('/'); ?>common/img/about_off.gif'"></a></li>
        <li><a href="<?php echo home_url('/'); ?>about#feature"><img src="<?php echo home_url('/'); ?>common/img/feature_off.gif" height="89" width="17" alt="特徴と効能" onmouseover="this.src='<?php echo home_url('/'); ?>common/img/feature_on.gif'" onmouseout="this.src='<?php echo home_url('/'); ?>common/img/feature_off.gif'"></a></li>
        <li><a href="<?php echo home_url('/'); ?>method/"><img src="<?php echo home_url('/'); ?>common/img/method_off.gif" height="86" width="17" alt="入浴の仕方" onmouseover="this.src='<?php echo home_url('/'); ?>common/img/method_on.gif'" onmouseout="this.src='<?php echo home_url('/'); ?>common/img/method_off.gif'"></a></li>
        <li><a href="<?php echo home_url('/'); ?>shop#price"><img src="<?php echo home_url('/'); ?>common/img/price_off.gif" height="106" width="18" alt="料金のご案内" onmouseover="this.src='<?php echo home_url('/'); ?>common/img/price_on.gif'" onmouseout="this.src='<?php echo home_url('/'); ?>common/img/price_off.gif'"></a></li>
        <li><a href="<?php echo home_url('/'); ?>shop/"><img src="<?php echo home_url('/'); ?>common/img/shop_off.gif" height="105" width="17" alt="店舗のご案内" onmouseover="this.src='<?php echo home_url('/'); ?>common/img/shop_on.gif'" onmouseout="this.src='<?php echo home_url('/'); ?>common/img/shop_off.gif'"></a></li>
        <li><a href="<?php echo home_url('/'); ?>power/"><img src="<?php echo home_url('/'); ?>common/img/power_off.gif" height="106" width="16" alt="酵素のちから" onmouseover="this.src='<?php echo home_url('/'); ?>common/img/power_on.gif'" onmouseout="this.src='<?php echo home_url('/'); ?>common/img/power_off.gif'"></a></li>
      </ul>

      <!--rsp表示-->
      <ul id="menu" class="rsp">
        <li><a href="<?php echo home_url('/'); ?>about/" class="ophv">酵素風呂とは</a></li>
        <li><a href="<?php echo home_url('/'); ?>about#feature" class="ophv">特徴と効能</a></li>
        <li><a href="<?php echo home_url('/'); ?>method/" class="ophv">入浴の仕方</a></li>
        <li><a href="<?php echo home_url('/'); ?>shop#price" class="ophv">料金のご案内</a></li>
        <li><a href="<?php echo home_url('/'); ?>shop/" class="ophv">店舗のご案内</a></li>
        <li><a href="<?php echo home_url('/'); ?>power/" class="ophv">酵素のちから</a></li>
      </ul>
    </nav>