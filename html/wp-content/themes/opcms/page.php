<?php /* Template Name: page */ ?>
<?php
  $page_id = $post->ID; //xxxに 固定ページIDを入力
  $content = get_page($page_id);
  $this_pagetitle = $content->post_title;
  $this_pageslug = $content->post_name;
?>
<?php get_header(); ?>


    <main>
      <div id="wrap" class="btm <?php echo $this_pageslug; ?>">
<?php if(have_posts()): while(have_posts()): the_post(); ?>
<?php the_content(); ?>
<?php endwhile; endif; ?>

      </div><!--/#wrap .<?php echo $this_pageslug; ?>-->
    </main>

<?php get_footer(); ?>